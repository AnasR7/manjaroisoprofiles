[Appearance]
ColorScheme=DarkPastels
Font=Noto Mono,10,-1,5,50,0,0,0,0,0

[General]
Command=/bin/zsh
Name=Shell
Parent=FALLBACK/
TerminalRows=30

[Terminal Features]
BlinkingCursorEnabled=true
